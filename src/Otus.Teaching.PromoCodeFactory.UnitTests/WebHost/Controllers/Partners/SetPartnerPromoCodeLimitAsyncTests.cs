﻿using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using Xunit;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly DataContext _dbContext;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IRepository<Partner> _partnersRepositoryInMemory;
        private readonly PartnersController _partnersController;

        public IServiceCollection ServiceCollection { get; set; }
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            
            var dbOptBuilder = GetDbOptionsBuilder();
            _dbContext = new DataContext(dbOptBuilder.Options);
            _partnersRepositoryInMemory = new EfRepository<Partner>(_dbContext);
        }

        private static DbContextOptionsBuilder<DataContext> GetDbOptionsBuilder()
        {
            string dbName = Guid.NewGuid().ToString();

            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseInMemoryDatabase(dbName)
                   .UseInternalServiceProvider(serviceProvider);
            return builder;
        }

        public Guid partnerLimitGuid = Guid.NewGuid();
        public Partner CreateBasePartner(int numberIssuedPromoCodes,  DateTime? cancelDate = null)
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Лопаты",
                IsActive = true,
                NumberIssuedPromoCodes = numberIssuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = partnerLimitGuid,
                        CreateDate = DateTime.Today.AddDays(-30),
                        EndDate = DateTime.Today,
                        Limit = 100,
                        CancelDate = cancelDate
                    }
                }
            };

            return partner;
        }

        [Fact]
        // Если партнер не найден, то также нужно выдать ошибку 404:
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            //var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        // Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400:
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var stringEqual = "Данный партнер не активен";
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner(100);
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            var badObjectResult = Assert.IsType<string>(badRequestResult.Value);
            Assert.Equal(stringEqual, badObjectResult);
        }

        [Fact]
        // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes:
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsNumberIssuedPromoCodesSetZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner(100);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = 100
            });

           
            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var okRequestResult = Assert.IsType<CreatedAtActionResult>(result);
            var okObjectResult = Assert.IsType<Partner>(okRequestResult.Value);  
            Assert.Equal(0, okObjectResult.NumberIssuedPromoCodes);
        }

        [Fact]
        // Если лимит закончился, то количество не обнуляется:
        public async void SetPartnerPromoCodeLimitAsync_LimitIsCansel_ReturnsNumberIssuedPromoCodesNoSetZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var numberIssuedPromoCodes = 99;
            var partner = CreateBasePartner(numberIssuedPromoCodes, DateTime.Today.AddDays(-10));

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(-10),
                Limit = 100
            });
           
            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var okRequestResult = Assert.IsType<CreatedAtActionResult>(result);
            var okObjectResult = Assert.IsType<Partner>(okRequestResult.Value);  
            Assert.Equal(numberIssuedPromoCodes, okObjectResult.NumberIssuedPromoCodes);
        }

        [Fact]
        // При установке лимита нужно отключить предыдущий лимит:
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsDisableLastLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner(100);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = 100
            });

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var okRequestResult = Assert.IsType<CreatedAtActionResult>(result);
            var okObjectResult = Assert.IsType<Partner>(okRequestResult.Value);
            Assert.NotNull(okObjectResult.PartnerLimits.First(x => x.Id == partnerLimitGuid).CancelDate);
        }

        [Fact]
        // Лимит должен быть больше 0:
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsBadRequestLimitMustBeGreaterThanZero()
        {
            // Arrange
            var stringEqual = "Лимит должен быть больше 0";
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner(100);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = -1
            });

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            var badObjectResult = Assert.IsType<string>(badRequestResult.Value);
            Assert.Equal(stringEqual, badObjectResult);
        }
        [Fact]
        // Нужно убедиться, что сохранили новый лимит в базу данных:
        public async void SetPartnerPromoCodeLimitAsync_SaveLimit_ReturnsSavedelement()
        {
            // Arrange
            var partner = CreateBasePartner(100);
            await _partnersRepositoryInMemory.AddAsync(partner);
                                    
            // Act
            var result = await _partnersRepositoryInMemory.GetAllAsync();

            // Assert
            Assert.Single(result);
            Assert.Equal(partner, result.First());
        }
    }
}
